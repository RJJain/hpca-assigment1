#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
	register int *copy = new(nothrow) int[height * width];
	if( !copy )
	{
		cout << "Memory allocation failed\n";
		return;
	}
	register int h = 0, w = 0;
	register int offx, offy;
	register unsigned int current_row_first_index_filter1, current_row_first_index_filter2;
	register int row_index_to_process_for_filter2, col_index_to_process_for_filter2;
	register unsigned int image_index_filter2_processed;
	register int filter_weighted_sum;
	for(h = 0; h < height; ++h)
	{	
		row_index_to_process_for_filter2 = h - 1;
		current_row_first_index_filter1 = h * width;
		for(w = 0; w < width; ++w)
		{
			copy[ current_row_first_index_filter1 + w] = 0;

			/* Applying the FILTER1 on the original image to obtain elements in copy array*/
			filter_weighted_sum = 0;

			// offx - FILTER1 ROW ITERATION
			offx = -1;  // FILTER 1 ROW 1
			if(offx + h >= 0 ){
				
				// offy - FILTER1 COL ITERATION
				offy = -1;
				if(offy + w >= 0 ) {
					filter_weighted_sum += image[(offx + h) * width + offy + w] *
						filter[ (1 + offx) * FILTER_SIZE + 1 + offy];
				}

				offy = 0;
				filter_weighted_sum += image[(offx + h) * width + offy + w] *
					filter[ (1 + offx) * FILTER_SIZE + 1 + offy];

				offy = 1;
				if(offy + w < width ) {
					filter_weighted_sum += image[(offx + h) * width + offy + w] *
						filter[ (1 + offx) * FILTER_SIZE + 1 + offy];
				}
			}

			 offx = 0; // FILTER 1 ROW 2

			 // offy - FILTER1 COL ITERATION
			 offy = -1;
			 if(offy + w >= 0 ) {
				 filter_weighted_sum += image[(offx + h) * width + offy + w] *
					 filter[ (1 + offx) * FILTER_SIZE + 1 + offy];
			 }

			 offy = 0;
			 filter_weighted_sum += image[(offx + h) * width + offy + w] *
				 filter[ (1 + offx) * FILTER_SIZE + 1 + offy];

			 offy = 1;
			 if(offy + w < width ) {
				 filter_weighted_sum += image[(offx + h) * width + offy + w] *
					 filter[ (1 + offx) * FILTER_SIZE + 1 + offy];
			 }

			offx = 1; // FILTER 1 ROW 3
			if( offx + h < height) {

				// offy - FILTER1 COL ITERATION
				offy = -1;
				if(offy + w >= 0 ) {
					filter_weighted_sum += image[(offx + h) * width + offy + w] *
						filter[ (1 + offx) * FILTER_SIZE + 1 + offy];
				}

				offy = 0;
				filter_weighted_sum += image[(offx + h) * width + offy + w] *
					filter[ (1 + offx) * FILTER_SIZE + 1 + offy];

				offy = 1;
				if(offy + w < width ) {
					filter_weighted_sum += image[(offx + h) * width + offy + w] *
						filter[ (1 + offx) * FILTER_SIZE + 1 + offy];
				}
			}
			copy[ current_row_first_index_filter1 + w] = filter_weighted_sum;  // Putting the FILTER1 processed value in copy array element
			
			/* FILTER 2 PROCESSING FOR THE IMAGE INDEX[current_row - 1][current_col - 2]  */ 
			col_index_to_process_for_filter2 = w - 2;
			if( row_index_to_process_for_filter2 >= 0 && col_index_to_process_for_filter2 >= 0 ) {
				/* Applying FILTER2 on copy array to obtain the final image elements */			
				current_row_first_index_filter2 = row_index_to_process_for_filter2 * width;
				image_index_filter2_processed = current_row_first_index_filter2 + col_index_to_process_for_filter2;
				image[image_index_filter2_processed] = 0;
				filter_weighted_sum = 0;
				
				offx = -1; // FILTER 2 ROW 1
				if(offx + row_index_to_process_for_filter2 >= 0) {
					// offy - FILTER2 COL ITERATION
					offy = -1;
					if(offy + col_index_to_process_for_filter2 >= 0 ) {
						filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
							filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
					}

					offy = 0;
					filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
						filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];

					offy = 1;
					if(offy + col_index_to_process_for_filter2 < width) {
						filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
							filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
					}
				}

				offx = 0; // FILTER 2 ROW 2

				offy = -1;
				if(offy + col_index_to_process_for_filter2 >= 0 ) {
					filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
						filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
				}

				offy = 0;
				filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
					filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];

				offy = 1;
				if(offy + col_index_to_process_for_filter2 < width) {
					filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
						filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
				}
				

				offx = 1; // FILTER 2 ROW 3
				if(offx + row_index_to_process_for_filter2 < height) {

					offy = -1;
					if(offy + col_index_to_process_for_filter2 >= 0 ) {
						filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
							filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
					}

					offy = 0;
					filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
						filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];

					offy = 1;
					if(offy + col_index_to_process_for_filter2 < width) {
						filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
							filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
					}
				}
				image[image_index_filter2_processed] = filter_weighted_sum;			
			}
		} // End of width loop
		// We can process filter2 for image 1 Row up and the remaining 2 columns
		for( col_index_to_process_for_filter2 = width - 2; col_index_to_process_for_filter2 < width; col_index_to_process_for_filter2++ ) {
			if( row_index_to_process_for_filter2 >= 0 && col_index_to_process_for_filter2 >= 0 ) {
				image_index_filter2_processed = current_row_first_index_filter2 + col_index_to_process_for_filter2;
				image[image_index_filter2_processed] = 0;
				filter_weighted_sum = 0;

				offx = -1;
				if(offx + row_index_to_process_for_filter2 >= 0 ) {

					offy = -1;
					if( offy + col_index_to_process_for_filter2 >= 0 ) {
						filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
							filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
					}	
				
					offy = 0;
					filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
						filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];

					offy = 1;
					if(offy + col_index_to_process_for_filter2 < width) {
						filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
							filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
					}	
				}

				offx = 0;

				offy = -1;
				if( offy + col_index_to_process_for_filter2 >= 0 ) {
					filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
						filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
				}	
				
				offy = 0;
				filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
					filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];

				offy = 1;
				if(offy + col_index_to_process_for_filter2 < width) {
					filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
						filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
				}	
				
				offx = 1;
				if( offx + row_index_to_process_for_filter2 < height ) {

					offy = -1;
					if( offy + col_index_to_process_for_filter2 >= 0 ) {
						filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
							filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
					}	
				
					offy = 0;
					filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
						filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];

					offy = 1;
					if(offy + col_index_to_process_for_filter2 < width) {
						filter_weighted_sum += copy[(offx + row_index_to_process_for_filter2) * width + offy + col_index_to_process_for_filter2 ] *
							filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
					}	
				}
				image[image_index_filter2_processed] = filter_weighted_sum;
			}
		}
	}
	// Last One row 
	h = height - 1;
	current_row_first_index_filter2 = h * width;
	for(w = 0; w < width; ++w)
	{
		image_index_filter2_processed = current_row_first_index_filter2 + w;
		image[image_index_filter2_processed] = 0;
		filter_weighted_sum = 0;

		offx = -1;
		
		offy = -1;
		if(offy + w >= 0) {
			filter_weighted_sum += copy[(offx + h) * width + offy + w ] *
				filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
		}

		offy = 0;
		filter_weighted_sum += copy[(offx + h) * width + offy + w ] *
			filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];

		offy = 1;
		if(offy + w < width) {
			filter_weighted_sum += copy[(offx + h) * width + offy + w ] *
				filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
		}

		offx = 0;

		offy = -1;
		if(offy + w >= 0) {
			filter_weighted_sum += copy[(offx + h) * width + offy + w ] *
				filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
		}

		offy = 0;
		filter_weighted_sum += copy[(offx + h) * width + offy + w ] *
			filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];

		offy = 1;
		if(offy + w < width) {
			filter_weighted_sum += copy[(offx + h) * width + offy + w ] *
				filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
		}
		

		offx = 1;
		if(offx + h < height) {

			offy = -1;
			if(offy + w >= 0) {
				filter_weighted_sum += copy[(offx + h) * width + offy + w ] *
					filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
			}

			offy = 0;
			filter_weighted_sum += copy[(offx + h) * width + offy + w ] *
				filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];

			offy = 1;
			if(offy + w < width) {
				filter_weighted_sum += copy[(offx + h) * width + offy + w ] *
					filter[ FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
			}
		}
		image[image_index_filter2_processed] = filter_weighted_sum;
	}
	delete[] copy;
}

int main()	
{
	int ht, wd;
	cin >> ht >> wd;
	int *img = new int[ht * wd];
	for(int i = 0; i < ht; ++i)
		for(int j = 0; j < wd; ++j)
			cin >> img[i * wd + j];

	int filters = FILTERS;
	int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
	for(int i = 0; i < filters; ++i)
		for(int j = 0; j < FILTER_SIZE; ++j)
			for(int k = 0; k < FILTER_SIZE; ++k)
				cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];

	clock_t begin = clock();
	applyFilters(img, ht, wd, filter, filters);
	clock_t end = clock();
	cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
	ofstream fout("output.txt");
	for(int i = 0; i < ht; ++i)
	{
		for(int j = 0; j < wd; ++j)
			fout << img[i * wd + j] << " ";
		fout << "\n";
	}
}
